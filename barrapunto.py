#!/usr/bin/python3

# Juan Carlos Rasero Acebes
# Parse de barrapunto

from xml.sax.handler import ContentHandler # Reconocedores de sax
from xml.sax import make_parser
import sys
import string
import urllib.request

class CounterHandler(ContentHandler):
    def __init__ (self):
        self.inContent = 0
        self.theContent = ""
        self.en_noticia = False

    def startElement (self, name, attrs):

        if name == 'item': # Si la etiqueta empieza por item
            self.en_noticia = True # significa que estamos en una noticia

        elif name == 'title': # Si la etiqueta empieza por title
            if self.en_noticia: # y estamos en una noticia
                self.inContent = 1 # entonces significa que hay contenido que nos interesa

        elif name == 'link': # Si la etiqueta empieza por link
            if self.en_noticia: #
                self.inContent = 1

    def endElement (self, name):

        if self.en_noticia == True:
            if name == 'title':
                title = self.theContent
                print("<li> Title: " + title + "</li>")

            elif name == 'link':
                link = "<li> Link: <a href=" + self.theContent + ">" + self.theContent + "</a></li>"
                print(link)

            elif name == 'item':
                self.en_noticia = False

            if self.inContent:
                self.inContent = 0
                self.theContent = ""

    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars


NoticiaParser = make_parser() # Parse sax.
NoticiaHandler = CounterHandler() # Reconozco las cosas que me interesan.
NoticiaParser.setContentHandler(NoticiaHandler) # Le digo al parser generico que cosas me interesan

xmlFile = urllib.request.urlopen('http://barrapunto.com/index.rss')
NoticiaParser.parse(xmlFile)
